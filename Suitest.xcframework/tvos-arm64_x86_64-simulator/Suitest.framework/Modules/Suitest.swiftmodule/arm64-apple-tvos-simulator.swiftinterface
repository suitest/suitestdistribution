// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 6.0 effective-5.10 (swiftlang-6.0.0.9.10 clang-1600.0.26.2)
// swift-module-flags: -target arm64-apple-tvos12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name Suitest
// swift-module-flags-ignorable: -no-verify-emitted-module-interface
import AVKit
import CoreGraphics
import CoreMedia
import CryptoSwift
import DeveloperToolsSupport
import Foundation
import Fuzi
import ObjectiveC
import QuartzCore
import Starscream
import Swift
import SwiftUI
import SwiftyXML
import SystemConfiguration
import TVMLKit
import UIKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc public class SuitestLauncher : ObjectiveC.NSObject {
  @objc public static let instance: Suitest.SuitestLauncher
  @objc public func start()
  @objc public func stop()
  @objc deinit
}
public enum ReachabilityError : Swift.Error {
  case failedToCreateWithAddress(Darwin.sockaddr, Swift.Int32)
  case failedToCreateWithHostname(Swift.String, Swift.Int32)
  case unableToSetCallback(Swift.Int32)
  case unableToSetDispatchQueue(Swift.Int32)
  case unableToGetFlags(Swift.Int32)
}
@available(*, unavailable, renamed: "Notification.Name.reachabilityChanged")
public let ReachabilityChangedNotification: Foundation.NSNotification.Name
extension Foundation.NSNotification.Name {
  public static let reachabilityChanged: Foundation.Notification.Name
}
public class Reachability {
  public typealias NetworkReachable = (Suitest.Reachability) -> ()
  public typealias NetworkUnreachable = (Suitest.Reachability) -> ()
  @available(*, unavailable, renamed: "Connection")
  public enum NetworkStatus : Swift.CustomStringConvertible {
    case notReachable, reachableViaWiFi, reachableViaWWAN
    public var description: Swift.String {
      get
    }
    public static func == (a: Suitest.Reachability.NetworkStatus, b: Suitest.Reachability.NetworkStatus) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum Connection : Swift.CustomStringConvertible {
    case unavailable, wifi, cellular
    public var description: Swift.String {
      get
    }
    @available(*, deprecated, renamed: "unavailable")
    public static let none: Suitest.Reachability.Connection
    public static func == (a: Suitest.Reachability.Connection, b: Suitest.Reachability.Connection) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public var whenReachable: Suitest.Reachability.NetworkReachable?
  public var whenUnreachable: Suitest.Reachability.NetworkUnreachable?
  @available(*, deprecated, renamed: "allowsCellularConnection")
  final public let reachableOnWWAN: Swift.Bool
  public var allowsCellularConnection: Swift.Bool
  public var notificationCenter: Foundation.NotificationCenter
  @available(*, deprecated, renamed: "connection.description")
  public var currentReachabilityString: Swift.String {
    get
  }
  @available(*, unavailable, renamed: "connection")
  public var currentReachabilityStatus: Suitest.Reachability.Connection {
    get
  }
  public var connection: Suitest.Reachability.Connection {
    get
  }
  required public init(reachabilityRef: SystemConfiguration.SCNetworkReachability, queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main)
  convenience public init(hostname: Swift.String, queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main) throws
  convenience public init(queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main) throws
  @objc deinit
}
extension Suitest.Reachability {
  public func startNotifier() throws
  public func stopNotifier()
  @available(*, deprecated, message: "Please use `connection != .none`")
  public var isReachable: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .cellular`")
  public var isReachableViaWWAN: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .wifi`")
  public var isReachableViaWiFi: Swift.Bool {
    get
  }
  public var description: Swift.String {
    get
  }
}
extension UIKit.UIView {
  @_Concurrency.MainActor @preconcurrency public static func swizzle()
}
extension UIKit.UIView {
  @_Concurrency.MainActor @preconcurrency @objc override dynamic open func value(forUndefinedKey key: Swift.String) -> Any?
}
@objc public protocol SuitestVideoGrabbing : Suitest.SuitestPlayerGrabbing, Suitest.SuitestPlayerItemGrabbing {
}
@objc public protocol SuitestPlayerGrabbing {
  @objc var videoState: Suitest.SuitestVideoState { get }
}
@objc public protocol SuitestPlayerItemGrabbing {
  @objc var videoDuration: Swift.Int64 { get }
  @objc var videoPosition: Swift.Int64 { get }
  @objc var videoURL: Swift.String? { get }
  @objc var nextContentProposalURL: Swift.String? { get }
  @objc var hasExternalMetadata: Swift.Bool { get }
  @objc var hasNavigationMarkerGroups: Swift.Bool { get }
}
@objc public enum SuitestVideoState : Swift.Int {
  case finished
  case paused
  case reversing
  case playing
  case error
  case buffering
  case none
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
extension Suitest.SuitestVideoState : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
extension AVFoundation.AVPlayer : Suitest.SuitestPlayerGrabbing {
  @_Concurrency.MainActor @preconcurrency @objc dynamic public var videoState: Suitest.SuitestVideoState {
    @objc get
  }
}
extension AVFoundation.AVPlayerItem : Suitest.SuitestPlayerItemGrabbing {
  @_Concurrency.MainActor @preconcurrency @objc dynamic public var videoDuration: Swift.Int64 {
    @objc get
  }
  @_Concurrency.MainActor @preconcurrency @objc dynamic public var videoPosition: Swift.Int64 {
    @objc get
  }
  @_Concurrency.MainActor @preconcurrency @objc dynamic public var videoURL: Swift.String? {
    @objc get
  }
  @_Concurrency.MainActor @preconcurrency @objc dynamic public var nextContentProposalURL: Swift.String? {
    @objc get
  }
  @_Concurrency.MainActor @preconcurrency @objc dynamic public var hasExternalMetadata: Swift.Bool {
    @objc get
  }
  @_Concurrency.MainActor @preconcurrency @objc dynamic public var hasNavigationMarkerGroups: Swift.Bool {
    @objc get
  }
}
@available(*, unavailable, renamed: "Connection")
extension Suitest.Reachability.NetworkStatus : Swift.Equatable {}
@available(*, unavailable, renamed: "Connection")
extension Suitest.Reachability.NetworkStatus : Swift.Hashable {}
extension Suitest.Reachability.Connection : Swift.Equatable {}
extension Suitest.Reachability.Connection : Swift.Hashable {}
extension Suitest.SuitestVideoState : Swift.Equatable {}
extension Suitest.SuitestVideoState : Swift.Hashable {}
extension Suitest.SuitestVideoState : Swift.RawRepresentable {}
