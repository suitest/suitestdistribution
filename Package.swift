// swift-tools-version:5.4
// The swift-tools-version declares the minimum version of Swift required to build this package.


import PackageDescription
let package = Package(
    name: "Suitest",
    platforms: [
        .macOS(.v10_12), .tvOS(.v12), .iOS(.v12)
    ],
    products: [
        .library(
            name: "SuitestPackage",
            targets: ["SuitestPackage"]
        ),
        .library(
            name: "SuitestPackageIOS",
            targets: ["SuitestIOSPackage"]
        ),
    ],
    dependencies: [
        .package(name: "CryptoSwift", url: "https://github.com/krzyzanowskim/CryptoSwift.git", from: "1.8.1"),
        .package( url: "https://github.com/daltoniam/Starscream.git", from: "4.0.8"),
        .package( url: "https://github.com/chenyunguiMilook/SwiftyXML.git", from: "3.1.0"),
        .package( url: "https://github.com/cezheng/Fuzi.git", from: "3.1.3")
        ],
    targets: [
        .binaryTarget(
            name: "Suitest",
            path: "Suitest.xcframework"
        ),
        .binaryTarget(
            name: "Suitest_iOS",
            path: "Suitest_iOS.xcframework"
        ),
        .target(name: "SuitestPackage",
                dependencies:[
                    "Suitest",
                    .product(name: "CryptoSwift", package: "CryptoSwift"),
                    .product(name: "Starscream", package: "Starscream"),
                    .product(name: "SwiftyXML", package: "SwiftyXML"),
                    .product(name: "Fuzi", package: "Fuzi")
                ],
                path: "Classes"
            ),
        .target(name: "SuitestIOSPackage",
                dependencies:[
                    "Suitest_iOS",
                    .product(name: "CryptoSwift", package: "CryptoSwift"),
                    .product(name: "Starscream", package: "Starscream"),
                    .product(name: "SwiftyXML", package: "SwiftyXML"),
                    .product(name: "Fuzi", package: "Fuzi")
                ],
                path: "IOSClasses"
        )
        ])
