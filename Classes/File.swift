import Foundation
import Suitest

public struct SuitestPackage {
    public static func start() {
        print("Starting...")
        SuitestLauncher.instance.start()
    }
}

