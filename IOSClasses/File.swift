import Foundation
import Suitest_iOS

public struct SuitestPackage {
    public static func start() {
        print("Starting...")
        SuitestLauncher.instance.start()
    }
}

